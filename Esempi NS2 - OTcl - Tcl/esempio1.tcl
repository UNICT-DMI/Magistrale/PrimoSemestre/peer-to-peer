set ns [new Simulator]

set fd [open out.nam w]
$ns namtrace-all $fd

#Define a 'finish' procedure
proc finish {} {
        global ns fd
        $ns flush-trace     
        close $fd 		;#Close the NAM trace file
        exec nam out.nam &	;#Execute NAM on the trace file
        exit 0
}

set n0 [$ns node]
set n1 [$ns node]
$ns duplex-link $n0 $n1 2Mb 100ms DropTail
set agent1 [new Agent/TCP]
set agent2 [new Agent/TCPSink]
$ns attach-agent $n0 $agent1
$ns attach-agent $n1 $agent2
$ns connect $agent1 $agent2
set ftp [new Application/FTP]
$ftp attach-agent $agent1
$ns at 0.2 "$ftp start"
$ns at 20.0 "$ftp stop"
$ns at 20.1 "finish"
$ns run
