Class Sender -superclass Agent/Message

# Message format: "Addr Op SeqNo"
Sender instproc send-next {} {
	$self instvar seq_ agent_addr_
	$self send "$agent_addr_ send $seq_"
	puts "Sender sends msg $seq_"	
	incr seq_
	[Simulator info instances] after 0.1 "$self send-next"
}

Sender instproc recv msg {
	$self instvar agent_addr_
	set sdr [lindex $msg 0]
	set seq [lindex $msg 2]
	puts "\t\t\t\t\t\tSender gets ack $seq"
}

####################################################�
Class Receiver -superclass Agent/Message
	
Receiver instproc recv msg {
	$self instvar agent_addr_
	set sdr [lindex $msg 0]
	set seq [lindex $msg 2]
	puts "\t\t\tReceiver gets seq $seq"
	$self send "$agent_addr_ ack $seq"
}
	
# Create scheduler
set ns [new Simulator]


# Tracing per nam
set fd [open message.tr w]
$ns namtrace-all $fd
$ns color 3 Blue
$ns color 2 Red
$ns color 1 Green

proc finish {fd} {
	[Simulator info instances] flush-trace
	close $fd
	exit 0
}


############################################
# creazione della rete

set R(1) [$ns node]
set R(2) [$ns node]
set N(1) [$ns node]
set N(2) [$ns node]
set N(3) [$ns node]
set N(4) [$ns node]

$ns duplex-link $R(1) $R(2) 128kb 50ms DropTail
$ns duplex-link $N(1) $R(1) 0.5Mb 10ms DropTail
$ns duplex-link $N(2) $R(1) 0.5Mb 10ms DropTail
$ns duplex-link $N(3) $R(2) 0.5Mb 10ms DropTail
$ns duplex-link $N(4) $R(2) 0.5Mb 10ms DropTail
$ns queue-limit $R(1) $R(2) 5
$ns queue-limit $R(2) $R(1) 5
	
#perdite di pacchetti: na, nb identificano il link 
proc loss {rate na nb} {
	set ns [Simulator info instances]
	set em1 [new ErrorModel]
	$em1 unit EU_PKT		;# errori a livello di pacchetto
	$em1 set rate_ $rate
	$em1 ranvar [new RandomVariable/Uniform]
	$em1 drop-target [new Agent/Null]
	$ns lossmodel $em1 $na $nb	;#collega il lossmodel al link assegnato
	
	set em2 [new ErrorModel]
	$em2 unit EU_PKT		;# errori a livello di pacchetto
	$em2 set rate_ $rate
	$em2 ranvar [new RandomVariable/Uniform]
	$em2 drop-target [new Agent/Null]
	$ns lossmodel $em2 $nb $na	;#collega il lossmodel al link assegnato
}

########################### traffico esterno #############
set udp0 [new Agent/UDP]
$ns attach-agent $N(2) $udp0
set null0 [new Agent/Null]
$ns attach-agent $N(4) $null0
$ns connect $udp0 $null0
set exp0 [new Application/Traffic/Exponential]
$exp0 set rate_ 128k
$exp0 attach-agent $udp0
$udp0 set fid_ 1
$ns at 1.0 "$exp0 start"

######################### Agent message ####################
set sdr [new Sender]
$sdr set seq_ 0
$sdr set packetSize_ 1000
set rcvr [new Receiver]
$rcvr set packetSize_ 40
$ns attach-agent $N(1) $sdr
$ns attach-agent $N(3) $rcvr
$ns connect $sdr $rcvr
$sdr set fid_ 2
$rcvr set fid_ 3

$ns at 1.1 "$sdr send-next"


###################### Error model ###########################
set seed 5
set lossrate 0.1
ns-random $seed
loss $lossrate $R(1) $R(2)

#####################################�
$ns at 10.0 "finish $fd"
$ns run



