# genera un grafo di nodi connessi.

proc AddNode1L {arraynode numlink bw delay } {
  global ns 
  upvar $arraynode node
     
  set num [array size node]			;# nodi gia allocati
  set node($num) [$ns node]			;# crea un nuovo nodo
  if {$num == 0} { return }
  
  $ns duplex-link $node($num) $node([expr $num-1]) $bw $delay DropTail ;  #crea almeno un link
  if {$num >= 10 } { 				;# oltre i primi 10 nodi, crea numlink link, al massimo
    for {set i 1} {$i < $numlink} {incr i} {
      set j [randBetween 0 [expr $num -1]]		;# un nodo a caso gia esistente
      if { [NumNeighbors $node($j)] < $numlink && [CheckLink $node($num) $node($j)] == 0 } {        
        # controlla il numero di vicini di j e se esiste già il link 
        $ns duplex-link $node($num) $node($j) $bw $delay DropTail
      }
    }
  }
}

# genera una serie di nodi (arraynodeOut) da collegare ad un gruppo di 
# nodi già creati (arraynodeIn)

proc AddNode2L {arraynodeIn arraynodeOut  numlink bw delay } {
  global ns 
  upvar $arraynodeIn nodeIn
  upvar $arraynodeOut nodeOut
     
  set numIn [array size nodeIn]			;# nodi interni gia allocati
  set numOut [array size nodeOut]		;# nodi esterni gia allocati
  set nodeOut($numOut) [$ns node]		;# crea un nuovo nodo
  for {set i 0} {$i < $numlink} {incr i} {
    set j [randBetween 0 [expr $numIn -1]]		;# un nodo a caso gia esistente
    if { [CheckLink $nodeOut($numOut) $nodeIn($j)] == 0 } {        
        # controlla il numero di vicini di j e se esiste già il link 
        $ns duplex-link $nodeOut($numOut) $nodeIn($j) $bw $delay DropTail
    }
  }
}


  
# restituisce il numero di nodi adiacenti il nodo dato
proc NumNeighbors {node} {		
  return [llength [$node set neighbor_]]
}

# restituisce vero se esiste il link tra i nodi n1 e n2
proc CheckLink {n1 n2} {
  global ns
  set l [$n2 set id_]:[$n1 set id_]
  if { [lsearch [$ns array names link_] $l] == -1} { 
    return 0 
  } else {
    return 1 
  }
}

# restituisce un numero random intero compreso tra min e max, estremi compresi
proc randBetween {Min Max } {
   return [ expr $Min + round(rand() * ($Max - $Min)) ] 
}

###################################################################################
#  main

set ns [new Simulator]

array set nodeL1 {}
array set nodeL2 {}
array set nodeL3 {}

for { set x 0} {$x < 20} { incr x} {
  AddNode1L nodeL1 5 10Mb 10ms
}

for { set x 0} {$x < 100} { incr x} {
  AddNode2L nodeL1 nodeL2 5 10Mb 10ms
}

for { set x 0} {$x < 1000} { incr x} {
  AddNode2L nodeL2 nodeL3 5 10Mb 10ms
}

puts [$ns array names Node_]

$ns at 5.0 "exit"

$ns run





















