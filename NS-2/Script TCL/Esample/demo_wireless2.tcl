# Define options
set val(chan)           Channel/WirelessChannel    ;#Channel Type
set val(prop)           Propagation/TwoRayGround   ;# radio-propagation model
set val(netif)          Phy/WirelessPhy            ;# network interface type
set val(mac)            Mac/802_11                 ;# MAC type
set val(ll)             LL                         ;# link layer type
set val(ant)            Antenna/OmniAntenna        ;# antenna model
set val(ifqlen)         50                         ;# max packets in ifq
set val(rxPower)        1.11                      ;#  (in W)
set val(txPower)        1.11                      ;#  (in W)
set val(energymodel)    EnergyModel                ;# 
set val(initialenergy)  10                      ;#  (in Joule)
set val(sleeppower)     0.02                    ;# energia consumata in stato di sleep
set val(tp)             0.03                      ;# Energia consumata per la transizione dallo stato di sleep a quello di attivita'...
#set val(ifq)            Queue/DropTail/PriQueue    ;# interface queue type

# DumbAgent no routing!, AODV, DSDV, DSR 
set val(rp)	DSDV; # 
if { $val(rp) == "DSR" } {
set val(ifq)            CMUPriQueue
} else {
set val(ifq)            Queue/DropTail/PriQueue
}

set val(x)	500
set val(y)      500
set val(nn)	4				   ;# number of nodes

set ns [new Simulator]

set f [open out2.tr w]
$ns trace-all $f

set nf [open out2.nam w]
$ns namtrace-all-wireless $nf $val(x) $val(y)

$ns use-newtrace

# set up topography object
set topo       [new Topography]

$topo load_flatgrid $val(x) $val(y)

#
# Create God
#
create-god $val(nn)

set chan_1_ [new $val(chan)]

$ns node-config -adhocRouting $val(rp) \
                -llType $val(ll) \
                -macType $val(mac) \
                -ifqType $val(ifq) \
                -ifqLen $val(ifqlen) \
                -antType $val(ant) \
                -propType $val(prop) \
                -phyType $val(netif) \
                -topoInstance $topo \
                -agentTrace ON \
                -routerTrace ON \
                -macTrace ON \
                -movementTrace ON \
                -channel $chan_1_ \
		-energyModel $val(energymodel) \
 		-initialEnergy $val(initialenergy) \
 		-rxPower $val(rxPower) \
 		-txPower $val(txPower)



set n(0) [$ns node]
$n(0) set X_ 100.0
$n(0) set Y_ 200.0
$n(0) set Z_ 0.0
$ns initial_node_pos $n(0) 30

set n(1) [$ns node]
$n(1) set X_ 300.0
$n(1) set Y_ 200.0
$n(1) set Z_ 0.0
$ns initial_node_pos $n(1) 30

set n(2) [$ns node]
$n(2) set X_ 500.0
$n(2) set Y_ 200.0
$n(2) set Z_ 0.0
$ns initial_node_pos $n(2) 30

set n(3) [$ns node]
$n(3) set X_ 700.0
$n(3) set Y_ 200.0
$n(3) set Z_ 0.0
$ns initial_node_pos $n(3) 30

set tcp [new Agent/TCP]
set sink [new Agent/TCPSink]
$ns attach-agent $n(0) $tcp
$ns attach-agent $n(3) $sink
$ns connect $tcp $sink
set ftp [new Application/FTP]
$ftp attach-agent $tcp
$ns at 0.0 "$ftp start" 

$ns at 201.4 "finish"

proc finish {} {
        global ns f nf
        $ns flush-trace
        close $f
        close $nf

      puts "running nam..."
        exec nam out2.nam &
        exit 0
}

$ns run

