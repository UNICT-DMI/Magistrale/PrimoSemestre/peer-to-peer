set ns [new Simulator]
set lossrate 0.01

################################# classe derivata ###########################

Class myAgentApp -superclass Application
myAgentApp instproc init {maxBytes src procFine} {
  	$self set daRicevere_ $maxBytes;	# Byte ancora da ricevere
  	$self set fine_ $procFine;		# procedura da chiamare al termine
	$self set peer_ $src;			# agent applicativo corrispondente
	$self set done_ 0;			# se done=1 quando daRicevere<0
  	$self next 
}

myAgentApp instproc recv {bytes} {
  	$self instvar daRicevere_ ; 		# bytes ancora da ricevere
  	$self instvar fine_
	$self instvar peer_
	$self instvar done_
	if {$done_ } { 
		puts fatto
		return 
	}
  	incr daRicevere_ [expr 40 - $bytes ]; 	# decrementa; toglie 40B di intestazione
  	if { $daRicevere_ <= 0} {
		set $done_ 1
		$self stop	
		[$self set peer_] stop   		
		eval $fine_
  	}
}
###############################################################

#perdite di pacchetti: na, nb identificano il link 
proc loss {rate na nb} {
	set ns [Simulator info instances]

	set em1 [new ErrorModel]
	$em1 unit EU_PKT		;# errori a livello di pacchetto
	$em1 set rate_ $rate
	$em1 ranvar [new RandomVariable/Uniform]
	$em1 drop-target [new Agent/Null]
	$ns lossmodel $em1 $na $nb	;#collega il lossmodel al link assegnato

	set em2 [new ErrorModel]
	$em2 unit EU_PKT		;# errori a livello di pacchetto
	$em2 set rate_ $rate
	$em2 ranvar [new RandomVariable/Uniform]
	$em2 drop-target [new Agent/Null]
	$ns lossmodel $em2 $nb $na	;#collega il lossmodel al link assegnato
}

# Procedura chiamata al termine del primo trasferimento
proc Start2 { src2 } {
  set ns [Simulator info instances]
  puts " Avvio flusso 2: [$ns now]"
  $src2 start
}

# Procedura chiamata al termine del secondo trasferimento
proc Stop {} {
  set ns [Simulator info instances]
  puts " tempo finale: [$ns now]"
  exit 
}

##################################### main ####################################

# Definizione della rete
set n1 [$ns node]
set n2 [$ns node]
$ns duplex-link $n1 $n2 10Mb 10ms DropTail

global defaultRNG;		# generatore pseudocasuale
$defaultRNG seed 0;		# inizializza un seme random

loss $lossrate $n1 $n2

# Agent livello di trasporto

set snd1 [new Agent/TCP]
set snd2 [new Agent/TCP]
set rcv1 [new Agent/TCPSink]
set rcv2 [new Agent/TCPSink]

$ns attach-agent $n1 $snd1
$ns attach-agent $n1 $rcv1
$ns attach-agent $n2 $snd2
$ns attach-agent $n2 $rcv2

$ns connect $snd1 $rcv2
$ns connect $snd2 $rcv1

$snd1 set fid_ 1
$snd1 set packetSize_ 512
$snd2 set fid_ 2
$snd2 set packetSize_ 512

# Agent livello di applicativo
set ftp1 [new Application/FTP]
set ftp2 [new Application/FTP]
$ftp1 attach-agent $snd1
$ftp2 attach-agent $snd2

set flusso1 512000
set flusso2  512000

set DestApp1 [new myAgentApp $flusso1 $ftp1 "Start2 $ftp2"]

$DestApp1 attach-agent $rcv2
$DestApp1 start

set DestApp2 [new myAgentApp $flusso2 $ftp2 Stop]
$DestApp2 attach-agent $rcv1
$DestApp2 start

# schedulatore

$ns at 0.1 "$ftp1 send $flusso1"

# avvio
$ns run
