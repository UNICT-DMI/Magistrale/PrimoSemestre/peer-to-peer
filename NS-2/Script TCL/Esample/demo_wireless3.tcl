# Define options
set val(chan)           Channel/WirelessChannel    ;#Channel Type
set val(prop)           Propagation/TwoRayGround   ;# radio-propagation model
set val(netif)          Phy/WirelessPhy            ;# network interface type
set val(mac)            Mac/802_11                 ;# MAC type
set val(ll)             LL                         ;# link layer type
set val(ant)            Antenna/OmniAntenna        ;# antenna model
set val(ifqlen)         50                         ;# max packets in ifq
set val(rxPower)        1.11                      ;#  (in W)
set val(txPower)        1.11                      ;#  (in W)
set val(energymodel)    EnergyModel               ;# 
set val(initialenergy)  10                        ;#  (in Joule)
set val(sleeppower)     0.02                      ;# energia consumata in stato di sleep
set val(tp)             0.03                      ;# Energia consumata per la transizione dallo stato di sleep a quello di attivita'...
#set val(ifq)            Queue/DropTail/PriQueue    ;# interface queue type

# DumbAgent no routing!, AODV, DSDV, DSR 
set val(rp)	DSDV; # 
if { $val(rp) == "DSR" } {
set val(ifq)            CMUPriQueue
} else {
set val(ifq)            Queue/DropTail/PriQueue
}

set val(x)	700
set val(y)     	400
set val(nn)	7				   ;# number of nodes

set ns [new Simulator]

set f [open out3.tr w]
$ns trace-all $f

set nf [open out3.nam w]
$ns namtrace-all-wireless $nf $val(x) $val(y)

$ns use-newtrace

# set up topography object
set topo       [new Topography]

$topo load_flatgrid $val(x) $val(y)

#
# Create God
#
create-god $val(nn)

set chan_1_ [new $val(chan)]

$ns node-config -adhocRouting $val(rp) \
                -llType $val(ll) \
                -macType $val(mac) \
                -ifqType $val(ifq) \
                -ifqLen $val(ifqlen) \
                -antType $val(ant) \
                -propType $val(prop) \
                -phyType $val(netif) \
                -topoInstance $topo \
                -agentTrace ON \
                -routerTrace ON \
                -macTrace ON \
                -movementTrace ON \
                -channel $chan_1_ \
		-energyModel $val(energymodel) \
 		-initialEnergy $val(initialenergy) \
 		-rxPower $val(rxPower) \
 		-txPower $val(txPower)



set n(0) [$ns node]
$n(0) set X_ 100.0
$n(0) set Y_ 200.0
$n(0) set Z_ 0.0
$ns initial_node_pos $n(0) 30

set n(1) [$ns node]
$n(1) set X_ 350.0
$n(1) set Y_ 50.0
$n(1) set Z_ 0.0
$ns initial_node_pos $n(1) 30
$ns at 0.01 "$n(1) start"

set n(2) [$ns node]
$n(2) set X_ 600.0
$n(2) set Y_ 200.0
$n(2) set Z_ 0.0
$ns initial_node_pos $n(2) 30

set n(3) [$ns node]
$n(3) set X_ 250.0
$n(3) set Y_ 200.0
$n(3) set Z_ 0.0
$ns initial_node_pos $n(3) 30


set n(4) [$ns node]
$n(4) set X_ 400.0
$n(4) set Y_ 200.0
$n(4) set Z_ 0.0
$ns initial_node_pos $n(4) 30

set n(5) [$ns node]
$n(5) set X_ 200.0
$n(5) set Y_ 400.0
$n(5) set Z_ 0.0
$ns initial_node_pos $n(5) 30

set n(6) [$ns node]
$n(6) set X_ 350.0
$n(6) set Y_ 350.0
$n(6) set Z_ 0.0
$ns initial_node_pos $n(6) 30

set tcp [new Agent/TCP]
set sink [new Agent/TCPSink]
$ns attach-agent $n(0) $tcp
$ns attach-agent $n(2) $sink
$ns connect $tcp $sink
set ftp [new Application/FTP]
$ftp attach-agent $tcp

$ns at 10.1 "$ftp start" 
$ns at 10.5 "$n(3) setdest 120 10 400"
$ns at 15.00 "$ftp stop" 
 
$ns at 15.50 "$ftp start" 
#$ns at 6.0 "$n(3) setdest 10 120 400"
$ns at 18.00 "$ftp stop" 

$ns at 18.50 "$ftp start" 
$ns at 19.00 "$ftp stop" 






$ns at 19.1 "finish"

proc finish {} {
        global ns f nf val
        $ns flush-trace
        close $f
        close $nf

      puts "running nam..."
        exec nam out3.nam &
        exit 0
}

$ns run

