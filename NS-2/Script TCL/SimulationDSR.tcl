# Creazione di un set di 30 nodi connessi a random

#Create a simulator object
set ns [new Simulator]

#Open the NAM trace file
set nf [open TraceFile.nam w]
$ns namtrace-all $nf

#Define a 'finish' procedure
proc finish {} {
        global ns nf
        $ns flush-trace
        #Close the NAM trace file
        close $nf
        #Execute NAM on the trace file
        exec nam TraceFile.nam &
        exit 0
}

#Variabili
set nodes 30 ;#nodi
array set link_ {} ;#array delle adicenze
set nLink 7 ;#N° adiacenze 


# restituisce  vero se esiste il link
proc ChekLink { n1 n2 } {
	global ns
	set l [$n2 set id_]:[$n1 set id_]
	if { [lsearch[ $ns array names link_] $l ] == -1 }
	{
		return 0
	} else {
		return 1
	}
}
#numero intero random tra un max e un min
proc randBetween { Min Max } {
	return [ expr $Min + round ( rand() * ( $Max - $Min ) ) ]
}

#Procedura per la Creazione dei Nodi 
proc Createnode { link_ nodes bw delay } {
	global ns
	upvar $link_ node
	set num [array size node] ;#nodi gia allocati
	set node($num) [$ns node] ;#crea un nuovo nodo

	$ns duplex-link $node($num) $node([expr $num-1]) $bw $delay DropTail
	if { $num >= 10 }
	for { set i 0 } { $i < $numlink } { incr i } {
     	set j [randBetween 0 [expr $num -1]]
     	if { [NumNeighbors $node($j)] < $numlink && [ChekLink $node($num) $node($j)] == 0}
     	{
     		$ns duplex-link $node($num) $node($j) $bw $delay DropTail
     	}
     	
	}
}

proc AddNode2L { arraynodeIn arraynodeOut numLink bw delay } {
	global ns
	upvar $arraynodeIn nodeIn
	upvar $arraynodeOut nodeOut

	set numIn [array size nodeIn]
	set numOut [array size nodeOut]
	set num [array size node]
	for {set i 0} {$i < $numlink} {incr i} {
		for {set i 0} {$i < $numlink} {incr i} {
				set nodeOut($num) [$ns nodeOut]
		}
	}
}
puts "ho eseguito"
$ns at 1.0 "start"
$ns at 4.0 "stop"
$ns at 5.0 "finish"
$ns run





